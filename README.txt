 Integral Image
 
 Description:
   This program resizes the input image (img) using the integral image (II). To this 
   end, the integral image is first computed over the image, for then to resize the 
   image according to the cell size parameter (cs). To keep efficiency, this program 
   makes use of mex files.

 Steps:
   Steps to exucute the program:
     1. Run the prg_setup.m file to configure the program paths and compile the mex 
        files.
     2. Run the prg_integral_image.m file to resize a given input image. 

 Contact:
   Michael Villamizar
   mvillami-at-iri.upc.edu
   Institut de Robòtica i Informática Industrial CSIC-UPC
   Barcelona - Spain
   2014

